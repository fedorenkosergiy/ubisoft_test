﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using extensions;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditorInternal;
using UnityEngine;
using UObject = UnityEngine.Object;
using ElementCallbackDelegate = UnityEditorInternal.ReorderableList.ElementCallbackDelegate;

public abstract class BaseEditor<T> : Editor where T : UObject
{
	protected const string FIX_BUTTON_LABEL = "Fix";
	private const string SHOW_BASE_INSPECTOR_LABEL = "Show base inspector";
	private const int ASSETS_WORD_LENGTH = 6;
	private bool _isDirty = false;

	protected bool _showBaseInspector = false;

	public T Target
	{
		get { return (T)target; }
	}

	public override void OnInspectorGUI()
	{
		Color colorWas = GUI.color;
		GUI.color = Color.yellow;
		_showBaseInspector = EditorGUILayout.Toggle(SHOW_BASE_INSPECTOR_LABEL, _showBaseInspector);
		GUI.color = colorWas;
		if (_showBaseInspector)
		{
			base.OnInspectorGUI();
		}
		else
		{
			OnCustomInspectorGUI();
			CheckProperties();
			CheckIfObjectIsDirty();
		}
	}

	protected abstract void OnCustomInspectorGUI();

	protected abstract void CheckProperties();

	private void CheckIfObjectIsDirty()
	{
		if (_isDirty)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.HelpBox("Object was modified", MessageType.Info);
			if (GUILayout.Button("Save"))
			{
				if (Target is ScriptableObject)
				{
					AssetDatabase.SaveAssets();
				}
				else if (Target is MonoBehaviour)
				{
					EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
				}
				_isDirty = false;
			}
			EditorGUILayout.EndHorizontal();
		}
	}

	protected static PropertyInfo GetProperty<K>(Expression<Func<T, K>> selector)
	{
		Expression body = selector;
		if (body is LambdaExpression)
		{
			body = ((LambdaExpression)body).Body;
		}
		switch (body.NodeType)
		{
			case ExpressionType.MemberAccess:
				return (PropertyInfo)((MemberExpression)body).Member;
			default:
				throw new InvalidOperationException();
		}
	}

	protected virtual void EditProperty(PropertyInfo property)
	{
		if (property.PropertyType == typeof(string))
		{
			EditStringProperty(property);
		}
		else if (property.PropertyType == typeof(float))
		{
			EditFloatProperty(property);
		}
		else if (property.PropertyType == typeof(int))
		{
			EditIntProperty(property);
		}
		else if (property.PropertyType == typeof(AnimationCurve))
		{
			EditAnimationCurveProperty(property);
		}
		else if (property.PropertyType == typeof(SceneAsset))
		{
			EditSceneProperty(property);
		}
		else if (property.PropertyType == typeof(GameObject))
		{
			EditObjectProperty<GameObject>(property);
		}
		else if (property.PropertyType == typeof(Texture2D))
		{
			EditObjectProperty<Texture2D>(property);
		}
		else
		{
			throw new NotImplementedException(property.PropertyType.Name + " editing has not supported yet");
		}
	}

	private void EditStringProperty(PropertyInfo property)
	{
		string prevValue = GetValue<string>(property);
		string newValue = EditorGUILayout.DelayedTextField(property.Name.SplitCamelCase(), prevValue);
		if (prevValue != newValue)
		{
			MarkAsDirty();
		}
		SetValue(property, newValue);
	}

	private K GetValue<K>(PropertyInfo property)
	{
		return (K)property.GetValue(Target, null);
	}

	private void SetValue(PropertyInfo property, object value)
	{
		property.SetValue(Target, value, null);
	}

	protected void MarkAsDirty()
	{
		EditorUtility.SetDirty(target);
		_isDirty = true;
	}

	private void EditFloatProperty(PropertyInfo property)
	{
		float prevValue = GetValue<float>(property);
		float newValue = EditorGUILayout.FloatField(property.Name.SplitCamelCase(), prevValue);
		if (prevValue != newValue)
		{
			MarkAsDirty();
		}
		SetValue(property, newValue);
	}

	private void EditIntProperty(PropertyInfo property)
	{
		int prevValue = GetValue<int>(property);
		int newValue = EditorGUILayout.IntField(property.Name.SplitCamelCase(), prevValue);
		if (prevValue != newValue)
		{
			MarkAsDirty();
		}
		SetValue(property, newValue);
	}

	private void EditAnimationCurveProperty(PropertyInfo property)
	{
		AnimationCurve prevValue = GetValue<AnimationCurve>(property);
		AnimationCurve newValue = EditorGUILayout.CurveField(property.Name.SplitCamelCase(), prevValue);
		newValue = new AnimationCurve(newValue.keys);
		if (!IsAnimationCurvesEqual(prevValue, newValue))
		{
			MarkAsDirty();
		}
		SetValue(property, newValue);
	}

	private bool IsAnimationCurvesEqual(AnimationCurve a, AnimationCurve b)
	{
		bool result = a.keys.Length == b.keys.Length;
		if (result)
		{
			for (int i = 0; i < a.keys.Length; ++i)
			{
				if (!a.keys[i].Equals(b.keys[i]))
				{
					result = false;
					break;
				}
			}
		}
		return result;
	}

	private void EditSceneProperty(PropertyInfo property)
	{
		SceneAsset prevValue = GetValue<SceneAsset>(property);
		SceneAsset newValue = (SceneAsset)EditorGUILayout.ObjectField(property.Name.SplitCamelCase(), prevValue, typeof(SceneAsset), false);
		if (prevValue != newValue)
		{
			MarkAsDirty();
		}
		SetValue(property, newValue);
	}

	private void EditObjectProperty<K>(PropertyInfo property) where K : UObject
	{
		K prevValue = GetValue<K>(property);
		K newValue = (K)EditorGUILayout.ObjectField(property.Name.SplitCamelCase(), prevValue, typeof(K), true);
		if (prevValue != newValue)
		{
			MarkAsDirty();
		}
		SetValue(property, newValue);
	}

	protected void EditComponentProperty<K>(PropertyInfo property) where K : MonoBehaviour
	{
		K prevValue = GetValue<K>(property);
		K newValue = EditorGUILayout.ObjectField(property.Name.SplitCamelCase(), prevValue, typeof(K), true) as K;
		if (prevValue != newValue)
		{
			MarkAsDirty();
		}
		SetValue(property, newValue);
	}

	protected void DrawHolder<K>(List<K> list, Rect rect, int index, bool isActive, bool isFocused) where K : UObject
	{
		K current = list[index];
		if (current == null)
		{
			Vector2 size = new Vector2(rect.size.x, 16.0f);
			Vector2 position = new Vector2(rect.position.x, rect.position.y + 0.5f * (rect.size.y - size.y));
			Rect field = new Rect(position, size);
			K result = EditorGUI.ObjectField(field, string.Empty, current, typeof(K), false) as K;
			if (result != null)
			{
				list[index] = result;
				MarkAsDirty();
			}
		}
		else
		{
			EditorGUILayout.BeginHorizontal();

			Vector2 size = new Vector2(rect.size.x * 0.5f, 16.0f);
			Vector2 labelPosition = new Vector2(rect.position.x, rect.position.y + 0.5f * (rect.size.y - size.y));
			Rect label = new Rect(labelPosition, size);
			EditorGUI.LabelField(label, current.name);
			Vector2 buttonPosition = new Vector2(rect.position.x + size.x, rect.position.y + 0.5f * (rect.size.y - size.y));
			Rect button = new Rect(buttonPosition, size);
			if (GUI.Button(button, "Select"))
			{
				Selection.activeObject = current;
			}
			EditorGUILayout.EndHorizontal();
		}
	}

	protected void AddNullItem(ReorderableList list)
	{
		list.list.Add(null);
		MarkAsDirty(list);
	}

	protected void MarkAsDirty(ReorderableList list)
	{
		MarkAsDirty();
	}

	protected bool IsCurve01(AnimationCurve curve)
	{
		bool result = true;
		result &= curve.keys.Length >= 2;
		result &= curve.keys[0].time == 0.0f;
		result &= curve.keys[0].value >= 0.0f;
		result &= curve.keys[0].value <= 1.0f;
		result &= curve.keys[curve.keys.Length - 1].time == 1.0f;
		result &= curve.keys[curve.keys.Length - 1].value >= 0.0f;
		result &= curve.keys[curve.keys.Length - 1].value <= 1.0f;
		return result;
	}

	protected AnimationCurve FixCurve(AnimationCurve curve)
	{
		AnimationCurve result = FixKeysCount(curve);
		result = FixFirstKey(curve);
		result = FixLastKey(result);
		return result;
	}

	protected AnimationCurve FixKeysCount(AnimationCurve curve)
	{
		AnimationCurve result = curve;
		while (result.keys.Length < 2)
		{
			result.AddKey(0.0f, 0.0f);
		}
		return result;
	}

	protected AnimationCurve FixFirstKey(AnimationCurve curve)
	{
		return FixKeyByIndex(curve, 0, 0.0f);
	}

	protected AnimationCurve FixLastKey(AnimationCurve curve)
	{
		return FixKeyByIndex(curve, curve.keys.Length - 1, 1.0f);
	}

	protected AnimationCurve FixKeyByIndex(AnimationCurve curve, int index, float time)
	{
		AnimationCurve result = curve;
		Keyframe key = result.keys[index];
		key.time = time;
		key.value = Mathf.Clamp01(key.value);
		result.MoveKey(index, key);
		return result;
	}

	protected ReorderableList AssignReorderableList<T>(List<T> editableData, ElementCallbackDelegate drawElementCallback) where T : UObject
	{
		ReorderableList result = new ReorderableList(editableData, typeof(T));
		result.drawElementCallback += drawElementCallback;
		result.onAddCallback += AddNullItem;
		result.onReorderCallback += MarkAsDirty;
		result.onChangedCallback += MarkAsDirty;
		return result;
	}

	public static void PrepareLocalPathInProject(string path)
	{
		tools.Path.Prepare(GetProjectDirectoryPath(), path);
	}

	protected static string GetProjectDirectoryPath()
	{
		string result = Application.dataPath;
		result = result.Remove(result.Length - ASSETS_WORD_LENGTH - 1);
		return result;
	}
}