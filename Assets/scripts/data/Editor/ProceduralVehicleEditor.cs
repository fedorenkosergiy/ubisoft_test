﻿using UnityEditor;
using UnityEngine;

namespace data.editor
{
	[CustomEditor(typeof(ProceduralVehicle))]
	public class ProceduralVehicleEditor: VehicleEditor<ProceduralVehicle>
	{
		private const string MAX_SPEED_HAS_WRONG_VALUE_LABEL = "Max speed has wrong value";
		private const string ACCELERATION_DURATION_HAS_WRONG_VALUE_LABEL = "Acceleration duration has wrong value";
		private const string INHIBITION_DURATION_HAS_WRONG_VALUE_LABEL = "Inhibition duration has wrong value";
		private const string MAX_STEARING_ANGLE_HAS_WRONG_VALUE_LABEL = "Max stearing angle has wrong value";
		private const string PREFAB_IS_NOT_ASSIGNED_LABEL = "Prefab is not assigned";
		private const float DEFAULT_MAX_SPEED = 50.0f;
		private const float DEFAULT_ACCELERATION_DURATION = 5.0f;
		private const float DEFAULT_INHIBITION_DURATION = 5.0f;
		private const float DEFAULT_MAX_STEARING_ANGLE = 30.0f;
		private const float DEFAULT_MAX_STEERING_ANGULAR_SPEED = 45.0f;

		protected override void OnCustomInspectorGUI()
		{
			base.OnCustomInspectorGUI();
			EditProperty(GetProperty(n => n.MaxSpeedMPH));
			EditProperty(GetProperty(n => n.AccelerationDuration));
			EditProperty(GetProperty(n => n.Acceleration));
			EditProperty(GetProperty(n => n.InhibitionDuration));
			EditProperty(GetProperty(n => n.Inhibition));
			EditProperty(GetProperty(n => n.SteeringMaxAngleDependOnSpeed));
			EditProperty(GetProperty(n => n.MaxAbsoluteStearingAngle));
			EditProperty(GetProperty(n => n.SpeedDependOnCurrentAbsoluteSteeringAngle));
			EditProperty(GetProperty(n => n.MaxSteeringAngularSpeed));
			EditProperty(GetProperty(n => n.SteeringInfluenceDependOnSpeed));
			EditProperty(GetProperty(n => n.WheelRadius));
		}

		protected override void CheckProperties()
		{
			base.CheckProperties();
			CheckMaxSpeed();
			CheckAccelerationDuration();
			CheckInhibitionDuration();
			CheckMaxStearingAngle();
			CheckAcceleration();
			CheckInhibition();
			CheckSteeringMaxAngleDependOnSpeed();
			CheckSpeedDependOnCurrentAbsoluteSteeringAngle();
			CheckMaxSteeringAngularSpeed();
			CheckSteeringInfluenceDependOnSpeed();
		}

		private void CheckMaxSpeed()
		{
			if (Target.MaxSpeedMPH <= 0.0f)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox(MAX_SPEED_HAS_WRONG_VALUE_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.MaxSpeedMPH = DEFAULT_MAX_SPEED;
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckAccelerationDuration()
		{
			if (Target.AccelerationDuration <= 0.0f)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox(ACCELERATION_DURATION_HAS_WRONG_VALUE_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.AccelerationDuration = DEFAULT_ACCELERATION_DURATION;
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckInhibitionDuration()
		{
			if (Target.InhibitionDuration <= 0.0f)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox(INHIBITION_DURATION_HAS_WRONG_VALUE_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.InhibitionDuration = DEFAULT_INHIBITION_DURATION;
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckMaxStearingAngle()
		{
			if (Target.MaxAbsoluteStearingAngle <= 0.0f || Target.MaxAbsoluteStearingAngle > 90.0f)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox(MAX_STEARING_ANGLE_HAS_WRONG_VALUE_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.MaxAbsoluteStearingAngle = DEFAULT_MAX_STEARING_ANGLE;
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckAcceleration()
		{
			if (!IsCurve01(Target.Acceleration))
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox("wrong acceleration curve", MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.Acceleration = FixCurve(Target.Acceleration);
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckInhibition()
		{
			if (!IsCurve01(Target.Inhibition))
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox("wrong inhibition curve", MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.Inhibition = FixCurve(Target.Inhibition);
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}
		private void CheckSteeringMaxAngleDependOnSpeed()
		{
			if (!IsCurve01(Target.SteeringMaxAngleDependOnSpeed))
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox("wrong steering max angle depend on scpeed curve", MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.SteeringMaxAngleDependOnSpeed = FixCurve(Target.SteeringMaxAngleDependOnSpeed);
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckSpeedDependOnCurrentAbsoluteSteeringAngle()
		{
			if (!IsCurve01(Target.SpeedDependOnCurrentAbsoluteSteeringAngle))
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox("wrong speed depend on current absolute steering angle curve", MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.SpeedDependOnCurrentAbsoluteSteeringAngle = FixCurve(Target.SpeedDependOnCurrentAbsoluteSteeringAngle);
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckMaxSteeringAngularSpeed()
		{
			if (Target.MaxSteeringAngularSpeed <= 0.0f)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox("Max steering angular speed has wrong value", MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.MaxSteeringAngularSpeed = DEFAULT_MAX_STEERING_ANGULAR_SPEED;
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckSteeringInfluenceDependOnSpeed()
		{
			if (!IsCurve01(Target.SteeringInfluenceDependOnSpeed))
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox("wrong steering influence depend on speed curve", MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.SteeringInfluenceDependOnSpeed = FixCurve(Target.SteeringInfluenceDependOnSpeed);
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}
	}
}