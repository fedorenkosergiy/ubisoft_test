﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace data.editor
{
	[CustomEditor(typeof(Settings))]
	public class SettingsEditor : BaseEditor<Settings>
	{
		private const string VEHICLES_LABEL = "Vehicles";
		private const string LEVELS_LABEL = "Levels";
		private const string ASSET_HAS_WRONG_LOCATION_LABEL = "Asset has wrong location";

		private ReorderableList _vehicles = null;
		private ReorderableList _levels = null;

		private void OnEnable()
		{
			_vehicles = AssignReorderableList(Target.VehiclesEO, DrawVehicleElement);
			_levels = AssignReorderableList(Target.LevelsEO, DrawLevelElement);
		}

		private void DrawVehicleElement(Rect rect, int index, bool isActive, bool isFocused)
		{
			DrawHolder(Target.VehiclesEO, rect, index, isActive, isFocused);
		}

		private void DrawLevelElement(Rect rect, int index, bool isActive, bool isFocused)
		{
			DrawHolder(Target.LevelsEO, rect, index, isActive, isFocused);
		}

		protected override void OnCustomInspectorGUI()
		{
			EditorGUILayout.LabelField(VEHICLES_LABEL);
			_vehicles.DoLayoutList();
			EditorGUILayout.LabelField(LEVELS_LABEL);
			_levels.DoLayoutList();

			EditProperty(GetProperty(n => n.MainMenuSceneName));
		}

		protected override void CheckProperties()
		{
			CheckTargetPath();
		}

		private void CheckTargetPath()
		{
			string currentPath = AssetDatabase.GetAssetPath(target);
			if (currentPath != Settings.SettingsFilePathWithAssetsDirectory)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox(ASSET_HAS_WRONG_LOCATION_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					PrepareLocalPathInProject(Settings.SettingsFilePathWithAssetsDirectory);
					AssetDatabase.Refresh();
					AssetDatabase.MoveAsset(currentPath, Settings.SettingsFilePathWithAssetsDirectory);
				}
				EditorGUILayout.EndHorizontal();
			}
		}
	}
}