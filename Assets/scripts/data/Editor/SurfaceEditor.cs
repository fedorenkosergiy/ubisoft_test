﻿using UnityEditor;
using UnityEngine;

namespace data
{
	[CustomEditor(typeof(Surface))]
	public class SurfaceEditor : BaseEditor<Surface>
	{
		protected override void CheckProperties()
		{
			CheckOnSpeedInfluenceCurve();
		}

		protected override void OnCustomInspectorGUI()
		{
			EditProperty(GetProperty(n => n.TextureIndex));
			EditProperty(GetProperty(n => n.OnSpeedInfluence));
		}

		private void CheckOnSpeedInfluenceCurve()
		{
			if (!IsCurve01(Target.OnSpeedInfluence))
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox("wrong on speed influence curve", MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.OnSpeedInfluence = FixCurve(Target.OnSpeedInfluence);
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}
	}
}