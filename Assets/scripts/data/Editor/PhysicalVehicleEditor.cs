﻿using UnityEditor;

namespace data.editor
{
	[CustomEditor(typeof(PhysicalVehicle))]
	public class PhysicalVehicleEditor : VehicleEditor<PhysicalVehicle>
	{
	}
}