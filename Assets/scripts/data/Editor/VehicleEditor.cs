﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace data.editor
{
	public class VehicleEditor<T> : BaseEditor<T> where T: Vehicle
	{
		private const string SURFACES_LABEL = "Surfaces";
		private ReorderableList _surfaces = null;

		protected virtual void OnEnable()
		{
			_surfaces = AssignReorderableList(Target.Surfaces, DrawSurfaceElement);
		}

		private void DrawSurfaceElement(Rect rect, int index, bool isActive, bool isFocused)
		{
			DrawHolder(Target.Surfaces, rect, index, isActive, isFocused);
		}

		protected override void CheckProperties()
		{
			EditProperty(GetProperty(n => n.Prefab));
			EditProperty(GetProperty(n => n.PreviewPrefab));
		}

		protected override void OnCustomInspectorGUI()
		{
			CheckPrefab();
			CheckPreviewPrefab();
			EditorGUILayout.LabelField(SURFACES_LABEL);
			_surfaces.DoLayoutList();
		}

		private void CheckPrefab()
		{
			if (Target.Prefab == null)
			{
				EditorGUILayout.HelpBox("Prefab is not assigned", MessageType.Error);
			}
		}

		private void CheckPreviewPrefab()
		{
			if (Target.PreviewPrefab == null)
			{
				EditorGUILayout.HelpBox("Prefab Preview is not assigned", MessageType.Error);
			}
		}
	}
}