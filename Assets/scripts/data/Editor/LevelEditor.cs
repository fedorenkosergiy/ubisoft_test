﻿using System.Collections.Generic;
using components.checkpoints;
using components.scenes;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace data
{
	[CustomEditor(typeof(Level))]
	public class LevelEditor : BaseEditor<Level>
	{
		private const string GOLD_TIME_LIMIT_HAS_WRONG_VALUE_LABEL = "Gold time limit has wrong value";
		private const string SILVER_TIME_LIMIT_HAS_WRONG_VALUE_LABEL = "Silver time limit has wrong value";
		private const string SCEHE_NOT_ASSIGNED_LABEL = "Scene not assigned";

		private const float DEFAULT_GOLD_TIME_LIMIT = 10.0f;
		private const float DEFAULT_SILVER_TIME_LIMIT = 20.0f;
		private const float MIN_DIFFERENCE_BETWEEN_GOLDEN_AND_SILVER_TIME_LIMIT = 0.01f;
		
		private const string SPLAT_PROTOTYPE_GRASS_GUID = "c6e0767b1f8c34890ac245217f4b9731";
		private const string SPLAT_PROTOTYPE_SAND_GUID = "bfd675cc0db1d4656b75dc6d6ba91142";

		private const string LIGHT_PREFAB_GUID = "d80a0ce0b9fe47e4bb7ff593010abcd0";
		private const string CAMERA_PREFAB_GUID = "dc1865af1e3b9294ea7774e838d60374";


		protected override void OnCustomInspectorGUI()
		{
			EditProperty(GetProperty(n => n.GoldTimeLimit));
			EditProperty(GetProperty(n => n.SilverTimeLimit));
			EditProperty(GetProperty(n => n.Scene));
		}

		protected override void CheckProperties()
		{
			CheckGoldTimeLimit();
			CheckSilverTimeLimit();
			CheckScene();
		}

		private void CheckGoldTimeLimit()
		{
			if (Target.GoldTimeLimit <= 0.0f)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox(GOLD_TIME_LIMIT_HAS_WRONG_VALUE_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.GoldTimeLimit = DEFAULT_GOLD_TIME_LIMIT;
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckSilverTimeLimit()
		{
			if (Target.SilverTimeLimit <= Target.GoldTimeLimit)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.HelpBox(SILVER_TIME_LIMIT_HAS_WRONG_VALUE_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					Target.SilverTimeLimit = Target.GoldTimeLimit + MIN_DIFFERENCE_BETWEEN_GOLDEN_AND_SILVER_TIME_LIMIT;
					MarkAsDirty();
				}
				EditorGUILayout.EndHorizontal();
			}
		}

		private void CheckScene()
		{
			EditorGUILayout.BeginHorizontal();
			if (Target.Scene == null)
			{
				EditorGUILayout.HelpBox(SCEHE_NOT_ASSIGNED_LABEL, MessageType.Error);
				if (GUILayout.Button(FIX_BUTTON_LABEL))
				{
					CreateScene();
				}
			}
			EditorGUILayout.EndHorizontal();
		}

		private void CreateScene()
		{
			Scene scene;
			if (CreateSceneFile(out scene))
			{
				Target.Scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
				CreateTerrain(scene.path);
				CreateCheckPoints();
				InstantiateLighPrefab();
				InstantiateCameraPrefab();
				InstantiateSpawnPoint();
				TryToAddSceneToBuildSettings(scene.path);
				EditorUtility.SetDirty(Target);
				EditorSceneManager.SaveScene(scene);
				AssetDatabase.SaveAssets();
			}
		}

		private bool CreateSceneFile(out Scene scene)
		{
			Scene s = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
			bool saved = EditorSceneManager.SaveScene(s);
			scene = s;
			return saved;
		}

		private void CreateTerrain(string scenePath)
		{
			TerrainData terrainData = new TerrainData();

			terrainData.SetDetailResolution(500, 500);
			terrainData.baseMapResolution = 512;
			terrainData.heightmapResolution = 513;
			terrainData.alphamapResolution = 512;
			terrainData.SetDetailResolution(512, 32);

			terrainData.name = Target.SceneName;

			SplatPrototype grass = new SplatPrototype();
			grass.metallic = 1.0f;
			grass.tileSize = Vector2.one;
			grass.texture = GetAssetByGUID<Texture2D>(SPLAT_PROTOTYPE_GRASS_GUID);

			SplatPrototype sand = new SplatPrototype();
			sand.metallic = 1.0f;
			sand.tileSize = Vector2.one;
			sand.texture = GetAssetByGUID<Texture2D>(SPLAT_PROTOTYPE_SAND_GUID);

			terrainData.splatPrototypes = new SplatPrototype[]
				{
					grass,
					sand
				};

			terrainData.size = new Vector3(500, 500, 500);
			Terrain terrain = (Terrain.CreateTerrainGameObject(terrainData)).GetComponent<Terrain>();
			terrain.Flush();

			terrain.name = Target.SceneName;
			terrain.transform.position = new Vector3(-250.0f, 0.0f, -250.0f);

			string path = System.IO.Path.ChangeExtension(scenePath, tools.Path.ASSET_EXTENSION);
			AssetDatabase.CreateAsset(terrainData, path);
		}

		private T GetAssetByGUID<T>(string guid) where T : Object
		{
			string path = AssetDatabase.GUIDToAssetPath(guid);
			return AssetDatabase.LoadAssetAtPath<T>(path);
		}

		private void CreateCheckPoints()
		{
			StartPoint start = CheckpointEditor.CreateCheckpoint<StartPoint>();
			start.name = "START";
			FinalPoint final = CheckpointEditor.CreateCheckpoint<FinalPoint>();
			final.name = "FINISH";
			start.Next = final;
		}

		private void InstantiateLighPrefab()
		{
			string path = AssetDatabase.GUIDToAssetPath(LIGHT_PREFAB_GUID);
			GameObject asset = AssetDatabase.LoadAssetAtPath<GameObject>(path);
			Object instance = PrefabUtility.InstantiatePrefab(asset);
			instance.name = asset.name;
		}

		private void InstantiateCameraPrefab()
		{
			string path = AssetDatabase.GUIDToAssetPath(CAMERA_PREFAB_GUID);
			GameObject asset = AssetDatabase.LoadAssetAtPath<GameObject>(path);
			Object instance = PrefabUtility.InstantiatePrefab(asset);
			instance.name = asset.name;
		}

		private void InstantiateSpawnPoint()
		{
			GameObject go = new GameObject("SPAWN POINT");
			go.transform.position = Vector3.zero;
			go.transform.rotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
			GameScene component = go.AddComponent<GameScene>();
			component.SpawnPoint = go.transform;
		}

		private void TryToAddSceneToBuildSettings(string scenePath)
		{
			if (ShouldAddSceneToBuildSettings(scenePath))
			{
				List<EditorBuildSettingsScene> scenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
				EditorBuildSettingsScene settingsScene = new EditorBuildSettingsScene();
				settingsScene.enabled = true;
				settingsScene.path = scenePath;
				scenes.Add(settingsScene);
				EditorBuildSettings.scenes = scenes.ToArray();
			}
		}

		private bool ShouldAddSceneToBuildSettings(string scenePath)
		{
			bool result = true;
			for (int i = 0; i < EditorBuildSettings.scenes.Length; ++i)
			{
				if (EditorBuildSettings.scenes[i].path == scenePath)
				{
					result = false;
					break;
				}
			}
			return result;
		}
	}
}