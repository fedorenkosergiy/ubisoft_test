﻿using System.Collections.Generic;
using UnityEngine;
using tools;
namespace data
{
	[CreateAssetMenu(fileName = "settings", menuName = "settings")]
	public class Settings : ScriptableObject
	{
		private const string FILE_NAME = "settings";
		private const string TARGET_PATH = "Resources/" + FILE_NAME + ".asset";
		private const string TARGET_PATH_WITH_ASSETS_DIRECTORY = "Assets/" + TARGET_PATH;

		private static Settings _instance = null;

		[SerializeField]
		private List<Vehicle> _vehicles = new List<Vehicle>();
		[SerializeField]
		private List<Level> _levels = new List<Level>();
		[SerializeField]
		private string _mainMenuSceneName = "main_menu";

		public static Settings Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = Resources.Load<Settings>(FILE_NAME);
#if UNITY_EDITOR
					if (_instance == null)
					{
						_instance = CreateInstance<Settings>();
						_instance.name = FILE_NAME;
						Path.Prepare(Application.dataPath, TARGET_PATH);
						UnityEditor.AssetDatabase.CreateAsset(_instance, TARGET_PATH);
						UnityEditor.AssetDatabase.SaveAssets();
						UnityEditor.AssetDatabase.Refresh();
					}
#endif
				}
				return _instance;
			}
		}

		public string MainMenuSceneName
		{
			get { return _mainMenuSceneName; }
#if UNITY_EDITOR
			set { _mainMenuSceneName = value; }
#endif
		}

		public List<Level> LevelsCopy
		{
			get
			{
				return new List<Level>(_levels);
			}
		}

		public List<Vehicle> VehiclesCopy
		{
			get
			{
				return new List<Vehicle>(_vehicles);
			}
		}

#if UNITY_EDITOR

		public List<Vehicle> VehiclesEO
		{
			get { return _vehicles; }
			set { _vehicles = value; }
		}

		public List<Level> LevelsEO
		{
			get { return _levels; }
			set { _levels = value; }
		}

		public static string SettingsFilePathWithAssetsDirectory
		{
			get { return TARGET_PATH_WITH_ASSETS_DIRECTORY; }
		}
#endif
	}
}