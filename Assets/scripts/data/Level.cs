﻿using UnityEngine;

namespace data
{
	[CreateAssetMenu(fileName = "level", menuName = "level")]
	public class Level : UniqueDataItem
	{
		[SerializeField]
		private string _sceneName = string.Empty;
		[SerializeField]
		private float _goldTimeLimit = 10.0f;
		[SerializeField]
		private float _silverTimeLimit = 20.0f;
#if UNITY_EDITOR
		[SerializeField]
		private UnityEditor.SceneAsset _scene = null;
#endif

		public virtual string SceneName
		{
			get { return _sceneName; }
		}

		public virtual float GoldTimeLimit
		{
			get { return _goldTimeLimit; }
			set { _goldTimeLimit = value; }
		}

		public virtual float SilverTimeLimit
		{
			get { return _silverTimeLimit; }
			set { _silverTimeLimit = value; }
		}

#if UNITY_EDITOR
		public virtual UnityEditor.SceneAsset Scene
		{
			get { return _scene; }
			set
			{
				_scene = value;
				if (_scene != null)
				{
					_sceneName = _scene.name;
				}
				else
				{
					_sceneName = string.Empty;
				}
			}
		}
#endif
	}
}