﻿using UnityEngine;

namespace data
{
	public struct LevelResult
	{
		public float Time;
		public Matrix4x4 [] Track;
	}
}