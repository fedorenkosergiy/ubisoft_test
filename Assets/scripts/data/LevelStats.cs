﻿using UnityEngine;
namespace data
{
	public struct LevelStats
	{
		public string LevelGUID;
		public float BestTime;
		public Matrix4x4 [] Track;
		public string VehicleGUID;
	}
}