﻿using System.Collections.Generic;
using UnityEngine;
namespace data
{
	public class Vehicle : UniqueDataItem
	{
		[SerializeField]
		private GameObject _prefab = null;
		[SerializeField]
		private GameObject _previewPrefab = null;
		[SerializeField]
		private List<Surface> _surfaces = new List<Surface>();
		private static AnimationCurve _defaultSurfaceOnSpeedInfluence = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 1.0f);

		public virtual GameObject Prefab
		{
			get { return _prefab; }
			set { _prefab = value; }
		}

		public virtual GameObject PreviewPrefab
		{
			get { return _previewPrefab; }
			set { _previewPrefab = value; }
		}

		public virtual List<Surface> Surfaces
		{
			get { return _surfaces; }
			set { _surfaces = value; }
		}

		public AnimationCurve GetSurfaceOnSppedInfluence(int index)
		{
			AnimationCurve result = _defaultSurfaceOnSpeedInfluence;
			for(int i = 0; i < _surfaces.Count; ++i)
			{
				if (_surfaces[i].TextureIndex == index)
				{
					result = _surfaces[i].OnSpeedInfluence;
					break;
				}
			}
			return result;
		}
	}
}