﻿using System;
using System.Collections.Generic;

namespace data
{
	public class Stats
	{
		private static Stats _instance = null;

		private Dictionary<string, LevelStats> _data = new Dictionary<string, LevelStats>();

		public LevelStats this[Level level]
		{
			get { return this[level.GUID]; }
			set { this[level.GUID] = value; }
		}

		public LevelStats this[string levelGUID]
		{
			get
			{
				if (Contains(levelGUID))
				{
					return _data[levelGUID];
				}
				else
				{
					throw new InvalidOperationException("LevelStats with LevelGUID=" + levelGUID + " not found");
				}
			}
			set
			{
				_data[levelGUID] = value;
			}
		}

		public static Stats Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new Stats();
				}
				return _instance;
			}
		}

		private Stats() { }

		public bool Contains(Level level)
		{
			return Contains(level.GUID);
		}

		public bool Contains(string levelGUID)
		{
			return _data.ContainsKey(levelGUID);
		}
	}
}