﻿using UnityEngine;
namespace data
{
	[CreateAssetMenu(fileName = "surface", menuName = "surface")]
	public class Surface : ScriptableObject
	{
		[SerializeField]
		private int _textureIndex = 0;
		[SerializeField]
		private AnimationCurve _onSpeedInfluence = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 0.5f);

		public int TextureIndex
		{
			get { return _textureIndex; }
			set { _textureIndex = value; }
		}

		public AnimationCurve OnSpeedInfluence
		{
			get { return _onSpeedInfluence; }
			set { _onSpeedInfluence = value; }
		}
	}
}