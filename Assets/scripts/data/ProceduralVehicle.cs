﻿using UnityEngine;
namespace data
{
	[CreateAssetMenu(fileName = "vehicle", menuName = "vehicle/procedural")]
	public class ProceduralVehicle : Vehicle
	{
		[SerializeField]
		private float _maxSpeedMPH = 50.0f;
		[SerializeField]
		private float _accelerationDuration = 5.0f;
		[SerializeField]
		private AnimationCurve _acceleration = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
		[SerializeField]
		private float _inhibitionDuration = 5.0f;
		[SerializeField]
		private AnimationCurve _inhibition = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 0.0f);
		[SerializeField]
		private AnimationCurve _stearingMaxAngleDependOnSpeed = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 0.5f);
		[SerializeField]
		private float _maxStearingAngle = 30.0f;
		[SerializeField]
		private AnimationCurve _speedDependOnCurrentAbsoluteStearingAngle = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 0.5f);
		[SerializeField]
		private float _maxSteeringAngularSpeed = 45.0f;
		[SerializeField]
		private AnimationCurve _steeringInfluenceDependOnSpeed = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 1.0f);
		[SerializeField]
		private float _wheelRadius = 0.35f;

		public virtual float MaxSpeedMPH
		{
			get { return _maxSpeedMPH; }
			set { _maxSpeedMPH = value; }
		}

		public virtual float AccelerationDuration
		{
			get { return _accelerationDuration; }
			set { _accelerationDuration = value; }
		}

		public virtual AnimationCurve Acceleration
		{
			get { return _acceleration; }
			set { _acceleration = value; }
		}

		public virtual float InhibitionDuration
		{
			get { return _inhibitionDuration; }
			set { _inhibitionDuration = value; }
		}

		public virtual AnimationCurve Inhibition
		{
			get { return _inhibition; }
			set { _inhibition = value; }
		}

		public virtual AnimationCurve SteeringMaxAngleDependOnSpeed
		{
			get { return _stearingMaxAngleDependOnSpeed; }
			set { _stearingMaxAngleDependOnSpeed = value; }
		}

		public virtual AnimationCurve SpeedDependOnCurrentAbsoluteSteeringAngle
		{
			get { return _speedDependOnCurrentAbsoluteStearingAngle; }
			set { _speedDependOnCurrentAbsoluteStearingAngle = value; }
		}

		public virtual float MaxAbsoluteStearingAngle
		{
			get { return _maxStearingAngle; }
			set { _maxStearingAngle = value; }
		}

		public virtual float MaxSteeringAngularSpeed
		{
			get { return _maxSteeringAngularSpeed; }
			set { _maxSteeringAngularSpeed = value; }
		}

		public virtual AnimationCurve SteeringInfluenceDependOnSpeed
		{
			get { return _steeringInfluenceDependOnSpeed; }
			set { _steeringInfluenceDependOnSpeed = value; }
		}

		public virtual float WheelRadius
		{
			get { return _wheelRadius; }
			set { _wheelRadius = value; }
		}
	}
}