﻿using System;
using UnityEngine;

public class UniqueDataItem : ScriptableObject
{
	[SerializeField]
	private string _guid = Guid.NewGuid().ToString();

	public virtual string GUID
	{
		get { return _guid; }
	}
}