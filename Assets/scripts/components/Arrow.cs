﻿using components.checkpoints;
using UnityEngine;
using UnityStandardAssets.Cameras;
namespace components
{
	public class Arrow : AbstractTargetFollower
	{
		protected override void FollowTarget(float deltaTime)
		{
			if (Target != null)
			{
				transform.position = Target.position;

				if (Checkpoint.ActiveCheckpoint.position != null)
				{
					Vector3 relativePos = Checkpoint.ActiveCheckpoint.position - transform.position;
					Vector3 ea = Quaternion.LookRotation(relativePos).eulerAngles;
					ea.x = 0.0f;
					ea.z = 0.0f;
					transform.eulerAngles = ea;
				}
			}
		}
	}
}