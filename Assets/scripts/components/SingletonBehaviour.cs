﻿using System;
using UnityEngine;

namespace components
{
	public class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
	{
		private static T _instance = null;

		public static T Instance
		{
			get { return _instance; }
		}

		protected virtual void Awake()
		{
			if (_instance == null)
			{
				_instance = (T)this;
			}
			else
			{
				throw new InvalidOperationException(typeof(T).ToString() + " has to be implemented just once");
			}
		}

		protected virtual void OnDestroy()
		{

		}
	}
}