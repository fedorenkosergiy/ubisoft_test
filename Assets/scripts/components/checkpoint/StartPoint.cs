﻿using System;
using UnityEngine;

namespace components.checkpoints
{
	public class StartPoint : Checkpoint
	{
		private static StartPoint _instance = null;

		public event Action<object, EventArgs> OnPassed = null;

		public static StartPoint Instance
		{
			get { return _instance; }
		}

		private void Awake()
		{
			if (_instance == null)
			{
				_instance = this;
			}
			else
			{
				throw new InvalidOperationException("StartPoint has to be included on scene just once");
			}
		}

		protected override void Start()
		{
			Activate();
		}

		private void OnDestroy()
		{
			_instance = null;
		}

		protected override void Pass()
		{
			base.Pass();
			if (OnPassed != null)
			{
				OnPassed(this, new EventArgs());
			}
			Debug.Log("Start");
		}

		protected override void OnDrawGizmos()
		{
			base.OnDrawGizmos();
			Gizmos.color = Color.red;
			Gizmos.DrawSphere(transform.position, 1);
		}
	}
}