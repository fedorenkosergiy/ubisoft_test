﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace components.checkpoints
{
	[CustomEditor(typeof(Checkpoint), true)]
	public class CheckpointEditor : BaseEditor<Checkpoint>
	{
		protected override void OnCustomInspectorGUI()
		{
			EditProperty(GetProperty(n => n.Next));
			EditProperty(GetProperty(n => n.ObjectIdle));
			EditProperty(GetProperty(n => n.ObjectActive));
			EditProperty(GetProperty(n => n.ObjectFeedback));
			TryAddNextCheckpoint();
		}

		protected override void EditProperty(PropertyInfo property)
		{
			if (property.PropertyType == typeof(Checkpoint))
			{
				EditComponentProperty<Checkpoint>(property);
			}
			else
			{
				base.EditProperty(property);
			}
		}

		protected virtual void TryAddNextCheckpoint()
		{
			if (GUILayout.Button("Add next"))
			{
				Checkpoint component = CreateCheckpoint<Checkpoint>();
				component.Next = Target.Next;
				Target.Next = component;
				Selection.activeObject = component.gameObject;
			}
		}

		public static T CreateCheckpoint<T>() where T:Checkpoint
		{
			GameObject go = new GameObject();
			BoxCollider collider = go.AddComponent<BoxCollider>();
			collider.isTrigger = true;
			T component = go.AddComponent<T>();

			GameObject idle = new GameObject("idle");
			idle.transform.parent = go.transform;
			component.ObjectIdle = idle;
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.SetParent(idle.transform);
			DestroyImmediate(cube.GetComponent<BoxCollider>());

			GameObject active = new GameObject("active");
			active.transform.parent = go.transform;
			component.ObjectActive = active;
			GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			sphere.transform.SetParent(active.transform);
			sphere.GetComponent<MeshRenderer>().material = null;
			DestroyImmediate(sphere.GetComponent<SphereCollider>());

			GameObject feedback = new GameObject("feedback");
			feedback.transform.parent = go.transform;
			component.ObjectFeedback = feedback;
			GameObject capsule = GameObject.CreatePrimitive(PrimitiveType.Capsule);
			capsule.transform.SetParent(feedback.transform);
			DestroyImmediate(capsule.GetComponent<CapsuleCollider>());
			return component;
		}

		protected override void CheckProperties()
		{
			CheckGraphicalFields();
			CheckNextNode();
		}

		protected virtual void CheckGraphicalFields()
		{
			if (Target.ObjectIdle == null)
			{
				EditorGUILayout.HelpBox("Field Object Idle not assigned", MessageType.Error);
			}
			if (Target.ObjectActive == null)
			{
				EditorGUILayout.HelpBox("Field Object Active not assigned", MessageType.Error);
			}
			if (Target.ObjectFeedback == null)
			{
				EditorGUILayout.HelpBox("Field Object Feedback not assigned", MessageType.Error);
			}
		}

		protected virtual void CheckNextNode()
		{
			if (Target.Next == null)
			{
				EditorGUILayout.HelpBox("Field Next not assigned", MessageType.Error);
			}
		}
	}
}