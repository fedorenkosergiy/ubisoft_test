﻿using UnityEditor;

namespace components.checkpoints
{
	[CustomEditor(typeof(FinalPoint), true)]
	public class FinalPointEditor : CheckpointEditor
	{
		protected override void OnCustomInspectorGUI()
		{
			EditProperty(GetProperty(n => n.ObjectIdle));
			EditProperty(GetProperty(n => n.ObjectActive));
			EditProperty(GetProperty(n => n.ObjectFeedback));
		}

		protected override void CheckNextNode() { }
	}
}