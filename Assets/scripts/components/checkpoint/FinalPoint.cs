﻿using System;
using UnityEngine;

namespace components.checkpoints
{
	public class FinalPoint : Checkpoint
	{
		private static FinalPoint _instance = null;

		public event Action<object, EventArgs> OnPassed = null;

		public static FinalPoint Instance
		{
			get { return _instance; }
		}

		private void Awake()
		{
			if (_instance == null)
			{
				_instance = this;
			}
			else
			{
				throw new InvalidOperationException("FinalPoint has to be included on scene just once");
			}
		}
		
		private void OnDestroy()
		{
			_instance = null;
		}

		protected override void Pass()
		{
			base.Pass();
			if (OnPassed != null)
			{
				OnPassed(this, new EventArgs());
			}
			Debug.Log("Finish");
		}

		protected override void OnDrawGizmos()
		{
			Gizmos.color = Color.green;
			Gizmos.DrawSphere(transform.position, 1);
		}
	}
}