﻿using UnityEngine;
namespace components.checkpoints
{
	[RequireComponent(typeof(Collider))]
	public class Checkpoint : MonoBehaviour
	{
		[SerializeField]
		protected Checkpoint _next = null;
		[SerializeField]
		protected GameObject _objectIdle = null;
		[SerializeField]
		protected GameObject _objectActive = null;
		[SerializeField]
		protected GameObject _objectFeedback = null;

		protected bool _isActive = false;
		protected bool _isPassed = false;
		protected static Transform _activeCheckpoint = null;

		public virtual bool IsPassed
		{
			get { return _isPassed; }
		}

#if UNITY_EDITOR
		public Checkpoint Next
		{
			get { return _next; }
			set { _next = value; }
		}

		public GameObject ObjectIdle
		{
			get { return _objectIdle; }
			set { _objectIdle = value; }
		}

		public GameObject ObjectActive
		{
			get { return _objectActive; }
			set { _objectActive = value; }
		}

		public GameObject ObjectFeedback
		{
			get { return _objectFeedback; }
			set { _objectFeedback = value; }
		}
#endif

		public static Transform ActiveCheckpoint
		{
			get
			{
				return _activeCheckpoint;
			}
		}

		protected virtual void Start()
		{
			_objectIdle.SetActive(true);
			_objectActive.SetActive(false);
			_objectFeedback.SetActive(false);
		}

		protected virtual void Activate()
		{
			_objectIdle.SetActive(false);
			_objectActive.SetActive(true);
			_objectFeedback.SetActive(false);
			_isActive = true;
			_activeCheckpoint = transform;
		}

		protected virtual void OnTriggerEnter(Collider other)
		{
			if (_isActive && other.tag == "Player")
			{
				Pass();
			}
		}

		protected virtual void Pass()
		{
			_objectIdle.SetActive(false);
			_objectActive.SetActive(false);
			_objectFeedback.SetActive(true);
			_isActive = false;
			_isPassed = true;
			if (_next != null)
			{
				_next.Activate();
			}
		}

		protected virtual void OnDrawGizmos()
		{
			if (_next != null)
			{
				Gizmos.color = Color.magenta;
				Gizmos.DrawLine(transform.position, _next.transform.position);
			}
		}
	}
}