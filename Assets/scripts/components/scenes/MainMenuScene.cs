﻿using UnityEngine;

namespace components.scenes
{
	public class MainMenuScene : SingletonBehaviour<MainMenuScene>
	{
		[SerializeField]
		private Transform _vehicleHolder = null;
		private GameObject _vehicle = null;

		public void InstantiateVehicle(GameObject prefab)
		{
			Destroy(_vehicle);
			_vehicle = Instantiate(prefab);
			_vehicle.transform.parent = _vehicleHolder;
			_vehicle.transform.localPosition = Vector3.zero;
			_vehicle.transform.localRotation = Quaternion.identity;
			_vehicle.transform.localScale = Vector3.one;
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			_vehicleHolder = null;
			_vehicle = null;
		}
	}
}