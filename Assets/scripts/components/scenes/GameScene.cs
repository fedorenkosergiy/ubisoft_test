﻿using UnityEngine;

namespace components.scenes
{
	public class GameScene : SingletonBehaviour<GameScene>
	{
		[SerializeField]
		private Transform _spawnPoint = null;
		private GameObject _vehicle = null;

#if UNITY_EDITOR
		public Transform SpawnPoint
		{
			set { _spawnPoint = value; }
		}
#endif

		public GameObject InstantiateVehicleOrGetInstance(GameObject prefab)
		{
			if (_vehicle == null)
			{
				_vehicle = Instantiate(prefab);
				_vehicle.transform.localScale = _spawnPoint.localScale;
				_vehicle.transform.position = _spawnPoint.position;
				_vehicle.transform.rotation = _spawnPoint.rotation;
			}
			return _vehicle;
		}

		private void OnDrawGizmos()
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(_spawnPoint.position, 1);
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			_spawnPoint = null;
			_vehicle = null;
		}
	}
}