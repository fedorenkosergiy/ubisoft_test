﻿using UnityEngine;

namespace components.vehicle
{
	public class SurfaceTypeDetector : MonoBehaviour
	{
		private TerrainData _terrainData = null;
		private Vector3 _terrainPosition = Vector3.zero;
		private Transform _transform = null;

		private void Start()
		{
			Terrain terrain = Terrain.activeTerrain;
			_terrainData = terrain.terrainData;
			_terrainPosition = terrain.transform.position;
			_transform = transform;
		}

		public float[] GetTextureMix()
		{
			int mapX = (int)(((_transform.position.x - _terrainPosition.x) / _terrainData.size.x) * _terrainData.alphamapWidth);
			mapX = Mathf.FloorToInt((int)Mathf.Clamp(mapX, 0.0f, _terrainData.alphamapWidth - 1));
			int mapZ = (int)(((_transform.position.z - _terrainPosition.z) / _terrainData.size.z) * _terrainData.alphamapHeight);
			mapZ = Mathf.FloorToInt(Mathf.Clamp(mapZ, 0.0f, _terrainData.alphamapHeight - 1));
			float[,,] splatmapData = _terrainData.GetAlphamaps(mapX, mapZ, 1, 1);
			float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];
			for (int n = 0; n < cellMix.Length; ++n)
			{
				cellMix[n] = splatmapData[0, 0, n];
			}
			return cellMix;
		}

		private void OnDestroy()
		{
			_terrainData = null;
		}
	}
}