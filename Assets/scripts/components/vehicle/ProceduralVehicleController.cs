﻿using data;
using extensions;
using tools;
using UnityEngine;
namespace components.vehicle
{
	public class ProceduralVehicleController : MonoBehaviour
	{
		private Rigidbody _rigidbody = null;
		[SerializeField]
		private ProceduralVehicle _parameters = null;
		[SerializeField]
		private Transform[] _steeringWheels = new Transform[] { };
		[SerializeField]
		private Transform[] _rotatingWheels = new Transform[] { };
		[SerializeField]
		private SurfaceTypeDetector[] _surfaceDetectors = new SurfaceTypeDetector[] { };

		private float _inhibitionDuration = 0.0f;
		private float _driveDuration = 0.0f;
		private float _maxSpeedMetersPerSecond = 0.0f;
		private float _maxReachedSpeed = 0.0f;
		private bool _isDriving = false;
		private float _currentSteeringAngle = 0.0f;
		private float _wheelCircleLenght = 0.0f;
		private float _singleDetectorInfluence = 1.0f;

		private void Start()
		{
			_maxSpeedMetersPerSecond = ValuesConvertor.MilesPerHourToMetersPerSecond(_parameters.MaxSpeedMPH);
			_inhibitionDuration = _parameters.InhibitionDuration;
			_rigidbody = GetComponent<Rigidbody>();
			_wheelCircleLenght = _parameters.WheelRadius * Mathf.PI * 2.0f;
			_singleDetectorInfluence = 1.0f / _surfaceDetectors.Length;
		}

		public void Move(float steering, float acceleration, float dt)
		{
			steering = Mathf.Clamp(steering, -1.0f, 1.0f);
			acceleration = Mathf.Clamp(acceleration, -1.0f, 1.0f);

			float speed = CalculateSpeed(acceleration, dt);
			speed = ModifySpeedWithBySurfaceInfluence(speed);
			float angle = CalculateSteeringAngle(speed, steering, dt);
			for (int i = 0; i < _steeringWheels.Length; ++i)
			{
				Vector3 angles = _steeringWheels[i].localEulerAngles;
				angles.y = angle;
				_steeringWheels[i].localEulerAngles = angles;
			}
			Vector3 currentVelocity = _rigidbody.velocity;
			currentVelocity = transform.forward * speed;
			currentVelocity.y = -0.01f;
			_rigidbody.velocity = currentVelocity;
			Vector3 localEulerAngles = transform.localEulerAngles;
			localEulerAngles.y += CalculateRealAngle(angle, speed, dt);
			transform.localEulerAngles = localEulerAngles;
			RotateWheels(dt);
		}

		private float CalculateSpeed(float acceleration, float dt)
		{
			float result = _rigidbody.velocity.magnitude;
			if (acceleration > 0.0f)
			{
				if (!_isDriving)
				{
					_inhibitionDuration = 0.0f;
					float speedProgress = result / _maxSpeedMetersPerSecond;
					_driveDuration = _parameters.AccelerationDuration * _parameters.Acceleration.GetTime(speedProgress);
					_isDriving = true;
				}

				float correctedDelta = dt * acceleration;
				_driveDuration += correctedDelta;

				if (result < _maxSpeedMetersPerSecond)
				{
					float timeProgress = _driveDuration / _parameters.AccelerationDuration;
					float accelerationProgress = _parameters.Acceleration.Evaluate(timeProgress);
					result = accelerationProgress * _maxSpeedMetersPerSecond;
				}
				else
				{
					result = _maxSpeedMetersPerSecond;
				}
				_maxReachedSpeed = result;
			}
			else
			{
				if (_isDriving)
				{
					_isDriving = false;
				}
				_inhibitionDuration += dt;
				if (_inhibitionDuration >= _parameters.InhibitionDuration)
				{
					_inhibitionDuration = _parameters.InhibitionDuration;
					result = 0.0f;
				}
				else
				{
					float timeProgress = _inhibitionDuration / _parameters.InhibitionDuration;
					float inhibitionProgress = _parameters.Inhibition.Evaluate(timeProgress);
					result = inhibitionProgress * _maxReachedSpeed;
				}

				if (result <= 0.0f && acceleration < 0.0f)
				{
					result = -2.0f;
				}
			}

			float steeringAngleProgress = Mathf.Abs(_currentSteeringAngle) / _parameters.MaxAbsoluteStearingAngle;
			float steeringInfluence = _parameters.SpeedDependOnCurrentAbsoluteSteeringAngle.Evaluate(steeringAngleProgress);
			result *= steeringInfluence;

			return result;
		}

		private float ModifySpeedWithBySurfaceInfluence(float speed)
		{
			float multiplier = 0.0f;
			float speedProgress = speed / _maxSpeedMetersPerSecond;

			for (int i = 0; i < _surfaceDetectors.Length; ++i)
			{
				float[] textureProportions = _surfaceDetectors[i].GetTextureMix();
				float influence = 1.0f;
				for (int j = 0; j < textureProportions.Length; ++j)
				{
					AnimationCurve influenceRule = _parameters.GetSurfaceOnSppedInfluence(j);
					influence *= Mathf.Clamp(textureProportions[j], 1.0f, influenceRule.Evaluate(speedProgress));
				}
				multiplier += influence * _singleDetectorInfluence;
			}
			return speed * multiplier;
		}

		private float CalculateSteeringAngle(float speed, float steering, float dt)
		{
			float result = 0.0f;

			float speedProgress = speed / _parameters.MaxSpeedMPH;
			float deltaAngle = 0.0f;
			float targetAngle = 0.0f;
			if (steering != 0.0f)
			{
				deltaAngle = dt * _parameters.MaxSteeringAngularSpeed * steering;
				targetAngle = _currentSteeringAngle + deltaAngle;
			}
			else
			{
				deltaAngle = dt * -_parameters.MaxSteeringAngularSpeed * Mathf.Sign(_currentSteeringAngle);
				if (Mathf.Abs(_currentSteeringAngle) < Mathf.Abs(deltaAngle))
				{
					targetAngle = 0.0f;
				}
				else
				{
					targetAngle = _currentSteeringAngle + deltaAngle;
				}
			}
			float maxAngle = 0.0f;
			if (speed > 0.0f)
			{
				maxAngle = _parameters.SteeringMaxAngleDependOnSpeed.Evaluate(speedProgress) * _parameters.MaxAbsoluteStearingAngle * Mathf.Sign(targetAngle);
			}
			else
			{
				maxAngle = _parameters.MaxAbsoluteStearingAngle * Mathf.Sign(targetAngle);
			}

			if (IsAngleOutOfBuunds(targetAngle, maxAngle))
			{
				result = maxAngle;
			}
			else
			{
				result = targetAngle;
			}
			_currentSteeringAngle = result;
			return result;
		}

		private bool IsAngleOutOfBuunds(float target, float bound)
		{
			return target > 0.0f && target > bound || target < 0.0f && target < bound;
		}

		private float CalculateRealAngle(float steeringAngle, float speed, float dt)
		{
			float result = 0.0f;
			float speedProgress = Mathf.Abs(speed) / _parameters.MaxSpeedMPH;
			float steeringInfluence = _parameters.SteeringInfluenceDependOnSpeed.Evaluate(speedProgress);
			result = steeringInfluence * steeringAngle * dt;
			result *= Mathf.Sign(speed);
			return result;
		}

		private void RotateWheels(float dt)
		{
			float speed = _rigidbody.velocity.z;
			float l = speed * dt;
			float rotationsCount = l / _wheelCircleLenght;
			float angle = rotationsCount * 2.0f * Mathf.PI * Mathf.Rad2Deg;
			for (int i = 0; i < _rotatingWheels.Length; ++i)
			{
				Vector3 angles = _rotatingWheels[i].localEulerAngles;
				angles.x += angle;
				_rotatingWheels[i].localEulerAngles = angles;
			}
		}
	}
}