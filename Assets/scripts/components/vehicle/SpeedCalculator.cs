﻿using UnityEngine;
namespace components.vehicle
{
	[RequireComponent(typeof(Rigidbody))]
	public class SpeedCalculator : MonoBehaviour
	{
		private Rigidbody _rigidbody = null;

		public float SpeedMetersPerSecond
		{
			get
			{
				float result = 0.0f;
				if (_rigidbody != null)
				{
					result = _rigidbody.velocity.magnitude;
				}
				return result;
			}
		}

		private void Start()
		{
			_rigidbody = GetComponent<Rigidbody>();
		}

		private void OnDestroy()
		{
			_rigidbody = null;
		}
	}
}