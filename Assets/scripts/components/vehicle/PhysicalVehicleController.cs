﻿using data;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
namespace components.vehicle
{
	public class PhysicalVehicleController : CarController
	{
		[SerializeField]
		private PhysicalVehicle _parameters = null;
		[SerializeField]
		private SurfaceTypeDetector[] _surfaceDetectors = new SurfaceTypeDetector[] { };
		private float _singleDetectorInfluence = 1.0f;

		private float _prevSpeed = 0.0f;
		private float _prevMultiplier = 1.0f;

		protected override void Start()
		{
			base.Start();
			_singleDetectorInfluence = 1.0f / _surfaceDetectors.Length;
		}

		protected override void CapSpeed()
		{
			base.CapSpeed();
			float speed = m_Rigidbody.velocity.magnitude;
			if (speed != _prevSpeed)
			{
				speed /= _prevMultiplier;
				_prevSpeed = speed;
			}
			float correctedSpeed = ModifySpeedWithBySurfaceInfluence(_prevSpeed);
			if (correctedSpeed < _prevSpeed)
			{
				m_Rigidbody.velocity = correctedSpeed * m_Rigidbody.velocity.normalized;
			}
		}

		private float ModifySpeedWithBySurfaceInfluence(float speed)
		{
			float multiplier = 0.0f;
			float speedProgress = speed / MaxSpeedMetersPerSecond;
			for (int i = 0; i < _surfaceDetectors.Length; ++i)
			{
				float[] textureProportions = _surfaceDetectors[i].GetTextureMix();
				float influence = 1.0f;
				for (int j = 0; j < textureProportions.Length; ++j)
				{
					AnimationCurve influenceRule = _parameters.GetSurfaceOnSppedInfluence(j);
					influence *= Mathf.Clamp(textureProportions[j], 1.0f, influenceRule.Evaluate(speedProgress));
				}
				multiplier += influence * _singleDetectorInfluence;
			}
			_prevMultiplier = multiplier;
			return speed * multiplier;
		}
	}
}