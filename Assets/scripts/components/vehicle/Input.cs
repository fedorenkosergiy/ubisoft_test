﻿using UnityEngine;
namespace components.vehicle
{
	[RequireComponent(typeof(ProceduralVehicleController))]
	public class Input : MonoBehaviour
	{
		private ProceduralVehicleController _controller = null;
		private void Start()
		{
			_controller = GetComponent<ProceduralVehicleController>();
		}

		private void FixedUpdate()
		{
#if UNITY_STANDALONE
			float steering = UnityEngine.Input.GetAxis("Horizontal");
			float acceleration = UnityEngine.Input.GetAxis("Vertical");

#elif UNITY_ANDROID
			float acceleration = UnityEngine.Input.touchCount > 0 ? 1.0f: 0.0f;
			float steering = UnityEngine.Input.acceleration.x;
#endif
			_controller.Move(steering, acceleration, Time.fixedDeltaTime);
		}
	}
}