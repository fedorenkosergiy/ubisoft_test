﻿using System;

namespace ui
{
	public delegate void SmartClick(object sender, SmartClickArgs e);

	public class SmartClickArgs : EventArgs
	{
		private ISmartUIItem _item = null;

		public ISmartUIItem Item
		{
			get { return _item; }
		}

		public SmartClickArgs(ISmartUIItem item)
		{
			_item = item;
		}
	}
}