﻿namespace ui
{
	public interface IMainMenu : IVisible
	{
		event Click OnExitClicked;
		event Click OnNextCarClicked;

		string CurrentCarName { set; }

		ISelectLevelPanel LevelsPanel { get; }
	}
}