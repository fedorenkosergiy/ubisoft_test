﻿using System.Collections.Generic;
using UnityEngine;

namespace ui.components.unity_ui
{
	public class SmartUIItem : MonoBehaviour, ISmartUIItem
	{
		private Dictionary<string, object> _userdata = new Dictionary<string, object>();

		public object GetUserdata(string key)
		{
			if (_userdata.ContainsKey(key))
			{
				return _userdata[key];
			}
			else
			{
				throw new KeyNotFoundException("Key " + key + " not found in smart item named " + name);
			}
		}

		public void SetUserdata(string key, object value)
		{
			_userdata[key] = value;
		}
	}
}