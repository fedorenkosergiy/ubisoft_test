﻿using UnityEngine;
using UnityEngine.UI;

namespace ui.components.unity_ui
{
	public class MainMenu : Visible, IMainMenu
	{
		[SerializeField]
		private SelectLevelPanel _levelsPanel = null;
		[SerializeField]
		private Button _buttonExit = null;
		[SerializeField]
		private Button _buttonNextCar = null;
		[SerializeField]
		private Text _carName = null;

		public event Click OnExitClicked;
		public event Click OnNextCarClicked;

		public ISelectLevelPanel LevelsPanel
		{
			get
			{
				return _levelsPanel;
			}
		}

		public string CurrentCarName
		{
			set
			{
				_carName.text = value;
			}
		}

		private void Start()
		{
			_buttonExit.onClick.AddListener(DoOnExitClicked);
			_buttonNextCar.onClick.AddListener(DoOnNextCarClicked);
		}

		private void DoOnExitClicked()
		{
			if (OnExitClicked != null)
			{
				OnExitClicked(this, new ClickArgs());
			}
		}

		private void DoOnNextCarClicked()
		{
			if (OnNextCarClicked != null)
			{
				OnNextCarClicked(this, new ClickArgs());
			}
		}
	}
}