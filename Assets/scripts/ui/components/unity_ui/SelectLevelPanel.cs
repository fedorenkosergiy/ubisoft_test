﻿using System.Collections.Generic;
using UnityEngine;

namespace ui.components.unity_ui
{
	public class SelectLevelPanel : MonoBehaviour, ISelectLevelPanel
	{
		[SerializeField]
		private Transform _itemsParent = null;
		[SerializeField]
		private LevelItem _itemPrefab = null;
		private List<LevelItem> _aliveItems = new List<LevelItem>();

		public event SmartClick OnSelectedLevel;

		public ILevelItem InstantiateItem()
		{
			LevelItem result = Instantiate(_itemPrefab);
			Transform t = result.transform;
			t.SetParent(_itemsParent);
			t.localScale = Vector3.one;
			t.localRotation = Quaternion.identity;
			t.localPosition = Vector3.zero;
			result.OnClicked += DoOnItemClicked;
			result.OnOrderChanged += DoOnOrderChanged;
			_aliveItems.Add(result);
			result.gameObject.SetActive(true);
			return result;
		}

		private void DoOnItemClicked(object sender, ClickArgs e)
		{
			if (OnSelectedLevel != null)
			{
				LevelItem item = (LevelItem)sender;
				OnSelectedLevel(this, new SmartClickArgs(item));
			}
		}

		private void DoOnOrderChanged(object sender, OrderChangedArgs e)
		{
			_aliveItems.Sort();
			for (int i = 0; i < _aliveItems.Count; ++i)
			{
				_aliveItems[i].transform.SetSiblingIndex(i);
			}
		}

		public void Clear()
		{
			for(int i = 0; i < _aliveItems.Count; ++i)
			{
				_aliveItems[i].OnClicked -= DoOnItemClicked;
				_aliveItems[i].OnOrderChanged -= DoOnOrderChanged;
				Destroy(_aliveItems[i].gameObject);
			}
			_aliveItems.Clear();
		}
	}
}