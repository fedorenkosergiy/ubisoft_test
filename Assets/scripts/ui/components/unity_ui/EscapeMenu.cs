﻿using UnityEngine;
using UnityEngine.UI;

namespace ui.components.unity_ui
{
	public class EscapeMenu : Visible, IEscapeMenu
	{
		[SerializeField]
		private Button _buttonResume = null;
		[SerializeField]
		private Button _buttonRestart = null;
		[SerializeField]
		private Button _buttonMainMenu = null;

		public event Click OnResumeClicked;
		public event Click OnRestartClicked;
		public event Click OnMainMenuClicked;

		private void Start()
		{
			_buttonResume.onClick.AddListener(DoOnResumeClicked);
			_buttonRestart.onClick.AddListener(DoOnRestartClicked);
			_buttonMainMenu.onClick.AddListener(DoOnMainMenuClicked);
		}

		private void DoOnResumeClicked()
		{
			if (OnResumeClicked != null)
			{
				OnResumeClicked(this, new ClickArgs());
			}
		}

		private void DoOnRestartClicked()
		{
			if (OnRestartClicked != null)
			{
				OnRestartClicked(this, new ClickArgs());
			}
		}

		private void DoOnMainMenuClicked()
		{
			if (OnMainMenuClicked != null)
			{
				OnMainMenuClicked(this, new ClickArgs());
			}
		}
	}
}