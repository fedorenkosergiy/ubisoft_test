﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ui.components.unity_ui
{
	public class LevelItem : SmartUIItem, ILevelItem, IComparable
	{
		[SerializeField]
		private Text _textName = null;
		[SerializeField]
		private ObjectsActivationController _threeStarsController = null;
		[SerializeField]
		private Button _buttonComponent = null;
		private int _order = 0;

		public event OrderChanged OnOrderChanged;
		public event Click OnClicked;

		public string Name
		{
			set
			{
				_textName.text = value;
			}
		}

		public ResultInStarsType BestResult
		{
			set
			{
				_threeStarsController.Refresh((int)value);
			}
		}

		public int Order
		{
			set
			{
				if (_order != value)
				{
					_order = value;
					DoOnOrderChanged();
				}
			}
		}

		private void Start()
		{
			_buttonComponent.onClick.AddListener(DoOnClicked);
		}

		private void DoOnOrderChanged()
		{
			if (OnOrderChanged != null)
			{
				OnOrderChanged(this, new OrderChangedArgs(_order));
			}
		}

		private void DoOnClicked()
		{
			if (OnClicked != null)
			{
				OnClicked(this, new ClickArgs());
			}
		}

		public int CompareTo(object obj)
		{
			LevelItem other = (LevelItem)obj;
			return _order.CompareTo(other._order);
		}
	}
}