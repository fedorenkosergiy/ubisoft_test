﻿using UnityEngine;
using UnityEngine.UI;
namespace ui.components.unity_ui
{
	[RequireComponent(typeof(CanvasScaler))]
	public class DPICalculator : MonoBehaviour
	{
		public void Start()
		{
#if UNITY_STANDALONE || UNITY_EDITOR
			GetComponent<CanvasScaler>().scaleFactor = 1.5f;
#else
			GetComponent<CanvasScaler>().scaleFactor = Screen.dpi / 160;
#endif
			Destroy(this);
		}
	}
}