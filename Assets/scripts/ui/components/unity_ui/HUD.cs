﻿using UnityEngine;
using UnityEngine.UI;

namespace ui.components.unity_ui
{
	public class HUD : Visible, IHUD
	{
		private const string TIME_FORMAT = "0.00";
		private const string SPEED_FORMAT = "0";

		[SerializeField]
		private Text _textTime = null;
		[SerializeField]
		private Text _textSpeed = null;
		[SerializeField]
		private Button _buttonEscape = null;
		private ResultInStarsType _availableResult = ResultInStarsType.Zero;

		public event Click OnEscapeClicked;

		public float TimeInSeconds
		{
			set
			{
				_textTime.text = value.ToString(TIME_FORMAT);
			}
		}

		public float Speed
		{
			set
			{
				_textSpeed.text = value.ToString(SPEED_FORMAT);
			}
		}

		public ResultInStarsType AvailableResult
		{
			set
			{
				if (_availableResult != value)
				{
					_availableResult = value;
					switch (_availableResult)
					{
						case ResultInStarsType.Three: _textTime.color = Color.green; break;
						case ResultInStarsType.Two: _textTime.color = Color.yellow; break;
						case ResultInStarsType.One: _textTime.color = Color.red; break;
						case ResultInStarsType.Zero: _textTime.color = Color.white; break;
					}
				}
			}
		}

		private void Start()
		{
			_buttonEscape.onClick.AddListener(DoOnEscapeClicked);
		}

		private void DoOnEscapeClicked()
		{
			if (OnEscapeClicked != null)
			{
				OnEscapeClicked(this, new ClickArgs());
			}
		}
	}
}