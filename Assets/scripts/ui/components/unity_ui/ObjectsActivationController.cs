﻿using UnityEngine;
namespace ui.components.unity_ui
{
	public class ObjectsActivationController : MonoBehaviour
	{
		[SerializeField]
		private GameObject[] _objects = new GameObject[3] { null, null, null };

		public void Refresh(int count)
		{
			Deactivate();
			for (int i = 0; i < count; ++i)
			{
				_objects[i].SetActive(true);
			}
		}

		private void Deactivate()
		{
			for (int i = 0; i < _objects.Length; ++i)
			{
				_objects[i].SetActive(false);
			}
		}
	}
}