﻿using UnityEngine;
using UnityEngine.UI;

namespace ui.components.unity_ui
{
	public class LevelResultScreen : Visible, ILevelResultScreen
	{
		private const string TIME_FORMAT = "0.00";
		[SerializeField]
		private ObjectsActivationController _threeStarsController = null;
		[SerializeField]
		private Text _textReachedTime = null;
		[SerializeField]
		private Text _textBestTime = null;
		[SerializeField]
		private Button _button = null;
		[SerializeField]
		private Text _textLevelName = null;

		public event Click OnClick;

		public ResultInStarsType Result
		{
			set
			{
				_threeStarsController.Refresh((int)value);
			}
		}

		public float ReachedTimeInSeconds
		{
			set
			{
				_textReachedTime.text = value.ToString(TIME_FORMAT);
			}
		}

		public float BestTimeInSeconds
		{
			set
			{
				_textBestTime.text = value.ToString(TIME_FORMAT);
			}
		}

		public string LevelName
		{
			set
			{
				_textLevelName.text = value;
			}
		}

		private void Start()
		{
			_button.onClick.AddListener(DoOnClick);
		}

		private void DoOnClick()
		{
			if (OnClick != null)
			{
				OnClick(this, new ClickArgs());
			}
		}
	}
}