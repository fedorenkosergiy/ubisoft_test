﻿using UnityEngine;
namespace ui.components.unity_ui
{
	public class UnityUIBehaviour : UI
	{
		[SerializeField]
		private MainMenu _mainMenu = null;
		[SerializeField]
		private HUD _hud = null;
		[SerializeField]
		private EscapeMenu _escapeMenu = null;
		[SerializeField]
		private LevelResultScreen _levelResultScreen = null;

		public override IMainMenu MainMenu
		{
			get { return _mainMenu; }
		}

		public override IHUD HUD
		{
			get { return _hud; }
		}

		public override IEscapeMenu EscapeMenu
		{
			get { return _escapeMenu; }
		}

		public override ILevelResultScreen LevelResultScreen
		{
			get { return _levelResultScreen; }
		}
	}
}