﻿using UnityEngine;
namespace ui.components.unity_ui
{
	public class Visible : MonoBehaviour, IVisible
	{
		[SerializeField]
		private GameObject _localRoot = null;

		public void Hide()
		{
			_localRoot.SetActive(false);
		}

		public void Show()
		{
			_localRoot.SetActive(true);
		}
	}
}