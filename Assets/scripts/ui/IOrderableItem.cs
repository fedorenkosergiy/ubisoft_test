﻿namespace ui
{
	public interface IOrderable
	{
		event OrderChanged OnOrderChanged;

		int Order { set; }
	}
}