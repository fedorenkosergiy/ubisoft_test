﻿namespace ui
{
	public interface ILevelResultScreen : IVisible
	{
		event Click OnClick;

		ResultInStarsType Result { set; }
		float ReachedTimeInSeconds { set; }
		float BestTimeInSeconds { set; }
		string LevelName { set; }
	}
}