﻿using components;
namespace ui
{
	public abstract class UI : SingletonBehaviour<UI>
	{
		public abstract IMainMenu MainMenu { get; }
		public abstract IHUD HUD { get; }
		public abstract IEscapeMenu EscapeMenu { get; }
		public abstract ILevelResultScreen LevelResultScreen { get; }
		
		public void Hide()
		{
			MainMenu.Hide();
			HUD.Hide();
			EscapeMenu.Hide();
			LevelResultScreen.Hide();
		}
	}
}