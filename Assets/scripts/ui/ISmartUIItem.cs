﻿namespace ui
{
	public interface ISmartUIItem
	{
		void SetUserdata(string key, object value);
		object GetUserdata(string key);
	}
}