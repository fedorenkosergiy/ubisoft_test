﻿namespace ui
{
	public interface IEscapeMenu : IVisible
	{
		event Click OnResumeClicked;
		event Click OnRestartClicked;
		event Click OnMainMenuClicked;
	}
}