﻿namespace ui
{
	public enum ResultInStarsType : int
	{
		Zero = 0,
		One = 1,
		Two = 2,
		Three = 3
	}
}