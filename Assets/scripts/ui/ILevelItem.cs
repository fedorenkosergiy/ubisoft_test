﻿namespace ui
{
	public interface ILevelItem : ISmartUIItem, IOrderable
	{
		event Click OnClicked;

		string Name { set; }
		ResultInStarsType BestResult { set; }
	}
}