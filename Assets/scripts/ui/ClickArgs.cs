﻿using System;

namespace ui
{
	public delegate void Click(object sender, ClickArgs e);

	public class ClickArgs : EventArgs { }
}