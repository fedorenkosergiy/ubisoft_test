﻿namespace ui
{
	public interface IVisible
	{
		void Show();
		void Hide();
	}
}