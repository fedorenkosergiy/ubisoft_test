﻿namespace ui
{
	public interface ISelectingPanel<T> where T : ISmartUIItem
	{
		event SmartClick OnSelectedLevel;
		T InstantiateItem();
		void Clear();
	}
}