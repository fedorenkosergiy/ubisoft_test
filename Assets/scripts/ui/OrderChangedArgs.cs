﻿using System;

namespace ui
{
	public delegate void OrderChanged(object sender, OrderChangedArgs e);

	public class OrderChangedArgs : EventArgs
	{
		private int _order = 0;
		
		public int Order
		{
			get { return _order; }
		}

		public OrderChangedArgs(int order)
		{
			_order = order;
		}
	}
}