﻿namespace ui
{
	public interface IHUD : IVisible
	{
		event Click OnEscapeClicked;

		float TimeInSeconds { set; }
		float Speed { set; }
		ResultInStarsType AvailableResult { set; }
	}
}