﻿using data;
using logic.core;
using ui;

namespace logic.nodes.main
{

	public abstract class MainLayerNode<T> : AbstractLogicNodeWithNextCandidade<T> where T : MainLayerNode<T>, new()
	{
		protected static Settings Settings
		{
			get { return Settings.Instance; }
		}

		protected static Stats Stats
		{
			get { return Stats.Instance; }
		}

		protected static IMainMenu UIMainMenu
		{
			get { return UI.Instance.MainMenu; }
		}

		protected static IHUD UIHUD
		{
			get { return UI.Instance.HUD; }
		}

		protected static IEscapeMenu UIEscapeMenu
		{
			get { return UI.Instance.EscapeMenu; }
		}

		protected static ILevelResultScreen UIResult
		{
			get { return UI.Instance.LevelResultScreen; }
		}

		public MainLayerNode() : base(LayerType.Main) { }

		public override void DoActivate()
		{
			UI.Instance.Hide();
		}

		public override void DoDeactivate()
		{
			NextNode = null;
		}
	}
}