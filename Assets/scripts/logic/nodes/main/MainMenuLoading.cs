﻿using components.scenes;
using logic.core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace logic.nodes.main
{
	public class MainMenuLoading : MainLayerNode<MainMenuLoading>
	{
		private AsyncOperation _loading = null;

		public override void DoActivate()
		{
			base.DoActivate();
			_loading = SceneManager.LoadSceneAsync(Settings.MainMenuSceneName);
			_loading.completed += _loading_completed;
		}

		private void _loading_completed(AsyncOperation obj)
		{
			NextNode = MainMenu.Instance;
		}

		public override ILogicNode DoUpdate(float dt)
		{
			if (MainMenuScene.Instance == null)
			{
				return null;
			}
			else
			{
				return base.DoUpdate(dt);
			}
		}

		public override void DoDeactivate()
		{
			_loading.completed -= _loading_completed;
			_loading = null;
		}
	}
}