﻿using UnityEngine;

namespace logic.nodes.main
{
	public class Pause : GameNode<Pause>
	{
		public override void DoActivate()
		{
			base.DoActivate();
			UIEscapeMenu.OnResumeClicked += UIEscapeMenu_OnResumeClicked;
			UIEscapeMenu.OnRestartClicked += UIEscapeMenu_OnRestartClicked;
			UIEscapeMenu.OnMainMenuClicked += UIEscapeMenu_OnMainMenuClicked;
			UIEscapeMenu.Show();
			Time.timeScale = 0.0f;
		}

		private void UIEscapeMenu_OnResumeClicked(object sender, ui.ClickArgs e)
		{
			Game.Instance.Switch(this);
		}

		private void UIEscapeMenu_OnRestartClicked(object sender, ui.ClickArgs e)
		{
			GameLoading.Instance.Switch(this);
		}

		private void UIEscapeMenu_OnMainMenuClicked(object sender, ui.ClickArgs e)
		{
			NextNode = MainMenuLoading.Instance;
		}

		public override void DoDeactivate()
		{
			base.DoDeactivate();
			UIEscapeMenu.OnResumeClicked -= UIEscapeMenu_OnResumeClicked;
			UIEscapeMenu.OnRestartClicked -= UIEscapeMenu_OnRestartClicked;
			UIEscapeMenu.OnMainMenuClicked -= UIEscapeMenu_OnMainMenuClicked;
			_level = null;
			Time.timeScale = 1.0f;
		}
	}
}