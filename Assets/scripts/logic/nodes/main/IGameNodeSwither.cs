﻿using logic.core;

namespace logic.nodes.main
{
	public interface IGameNodeSwither : IGameParametersHolder
	{
		ILogicNode NextNode { set; }
		void Switch(IGameNodeSwither source);
	}
}