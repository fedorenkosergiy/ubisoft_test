﻿using data;
using logic.core;

namespace logic.nodes.main
{
	public class GameNode<T> : MainLayerNode<T>, IGameNodeSwither where T : GameNode<T>, new()
	{
		protected Level _level = null;
		protected Vehicle _vehicle = null;

		public Level Level
		{
			get { return _level; }
			set { _level = value; }
		}

		public Vehicle Vehicle
		{
			get { return _vehicle; }
			set { _vehicle = value; }
		}

		protected LevelStats LevelStats
		{
			get { return Stats[_level]; }
			set { Stats[_level] = value; }
		}

		ILogicNode IGameNodeSwither.NextNode
		{
			set { NextNode = value; }
		}

		public void CopyParameters(IGameParametersHolder source)
		{
			_level = source.Level;
			_vehicle = source.Vehicle;
		}

		public void Switch(IGameNodeSwither source)
		{
			CopyParameters(source);
			source.NextNode = this;
		}

		public override void DoDeactivate()
		{
			base.DoDeactivate();
			_level = null;
			_vehicle = null;
		}
	}
}