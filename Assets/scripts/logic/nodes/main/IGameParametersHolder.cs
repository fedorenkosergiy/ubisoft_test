﻿using System.Collections.Generic;
using data;
using UnityEngine;

namespace logic.nodes.main
{
	public interface IGameParametersHolder
	{
		Level Level { get; set; }
		Vehicle Vehicle { get; set; }

		void CopyParameters(IGameParametersHolder source);
	}
}