﻿using components.scenes;
using logic.core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace logic.nodes.main
{
	public class GameLoading : GameNode<GameLoading>
	{
		private AsyncOperation _loading = null;

		public override void DoActivate()
		{
			base.DoActivate();
			_loading = SceneManager.LoadSceneAsync(_level.SceneName);
			_loading.completed += _loading_completed;
		}

		private void _loading_completed(AsyncOperation obj)
		{
			Game.Instance.Switch(this);
		}

		public override ILogicNode DoUpdate(float dt)
		{
			if (GameScene.Instance == null)
			{
				return null;
			}
			else
			{
				return base.DoUpdate(dt);
			}
		}

		public override void DoDeactivate()
		{
			base.DoDeactivate();
			_loading.completed -= _loading_completed;
			_loading = null;
			_level = null;
		}
	}
}