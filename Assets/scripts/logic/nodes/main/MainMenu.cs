﻿using System.Collections.Generic;
using components.scenes;
using data;
using ui;

namespace logic.nodes.main
{
	public class MainMenu : MainLayerNode<MainMenu>
	{
		private const string LEVEL_KEY = "level_e2c2adf938ed44718cb5ba88a5a06cad";

		private static int _vehicleIndex = 0;
		private List<Vehicle> _vehicles = null;

		public override void DoActivate()
		{
			base.DoActivate();
			UIMainMenu.OnExitClicked += UIMainMenu_OnExitClicked;
			UIMainMenu.LevelsPanel.OnSelectedLevel += LevelsPanel_OnSelectedLevel;
			UIMainMenu.OnNextCarClicked += UIMainMenu_OnNextCarClicked;
			LoadDataAboutLevels();
			_vehicles = Settings.VehiclesCopy;
			InstantiateCarPreview();
			UIMainMenu.Show();
		}

		private void UIMainMenu_OnExitClicked(object sender, ClickArgs e)
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			UnityEngine.Application.Quit();
#endif
		}

		private void LevelsPanel_OnSelectedLevel(object sender, SmartClickArgs e)
		{
			Level level = (Level)e.Item.GetUserdata(LEVEL_KEY);
			GameLoading.Instance.Level = level;
			GameLoading.Instance.Vehicle = _vehicles[_vehicleIndex];
			NextNode = GameLoading.Instance;
		}

		private void UIMainMenu_OnNextCarClicked(object sender, ClickArgs e)
		{
			++_vehicleIndex;
			if (_vehicles.Count == _vehicleIndex)
			{
				_vehicleIndex = 0;
			}
			InstantiateCarPreview();
		}

		private void InstantiateCarPreview()
		{
			UIMainMenu.CurrentCarName = _vehicles[_vehicleIndex].name;
			MainMenuScene.Instance.InstantiateVehicle(_vehicles[_vehicleIndex].PreviewPrefab);
		}

		private void LoadDataAboutLevels()
		{
			List<Level> levels = Settings.LevelsCopy;
			ILevelItem item = null;
			Level level = null;
			for (int i = 0; i < levels.Count; ++i)
			{
				level = levels[i];
				item = UIMainMenu.LevelsPanel.InstantiateItem();
				item.Order = i;
				item.Name = level.name;
				item.BestResult = CalculateStars(level);
				item.SetUserdata(LEVEL_KEY, level);
			}
		}

		private ResultInStarsType CalculateStars(Level level)
		{
			ResultInStarsType result = ResultInStarsType.Zero;
			if (Stats.Contains(level))
			{
				LevelStats stats = Stats[level];
				if (stats.BestTime <= level.GoldTimeLimit)
				{
					result = ResultInStarsType.Three;
				}
				else if (stats.BestTime <= level.SilverTimeLimit)
				{
					result = ResultInStarsType.Two;
				}
				else
				{
					result = ResultInStarsType.One;
				}
			}
			return result;
		}

		public override void DoDeactivate()
		{
			base.DoDeactivate();
			UIMainMenu.OnExitClicked -= UIMainMenu_OnExitClicked;
			UIMainMenu.LevelsPanel.OnSelectedLevel -= LevelsPanel_OnSelectedLevel;
			UIMainMenu.OnNextCarClicked -= UIMainMenu_OnNextCarClicked;
			ClearDataAboutLevels();
		}

		private void ClearDataAboutLevels()
		{
			UIMainMenu.LevelsPanel.Clear();
		}
	}
}