﻿namespace logic.nodes.main
{
	public class EntranceNode : MainLayerNode<EntranceNode>
	{
		public override void DoActivate()
		{
			NextNode = MainMenuLoading.Instance;
		}
	}
}