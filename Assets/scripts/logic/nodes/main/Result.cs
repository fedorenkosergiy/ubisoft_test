﻿using data;
using tools;

namespace logic.nodes.main
{
	public class Result : GameNode<Result>
	{
		private LevelResult _result;

		public LevelResult ResultInfo
		{
			set { _result = value; }
		}

		public override void DoActivate()
		{
			base.DoActivate();
			CreateStatsIfNeeded();
			UpdateStatsIfNeeded();
			UIResult.LevelName = _level.name;
			UIResult.Result = ValuesConvertor.TimeToStars(_level, _result.Time);
			UIResult.ReachedTimeInSeconds = _result.Time;
			UIResult.BestTimeInSeconds = LevelStats.BestTime;
			UIResult.OnClick += UIResult_OnClick;
			UIResult.Show();
		}

		private void CreateStatsIfNeeded()
		{
			if (!Stats.Contains(_level))
			{
				SaveLevelStats();
			}
		}

		private void SaveLevelStats()
		{
			LevelStats = new LevelStats
			{
				LevelGUID = _level.GUID,
				VehicleGUID = _vehicle.GUID,
				BestTime = _result.Time,
				Track = _result.Track
			};
		}

		private void UpdateStatsIfNeeded()
		{
			if (_result.Time < LevelStats.BestTime)
			{
				SaveLevelStats();
			}
		}

		private void UIResult_OnClick(object sender, ui.ClickArgs e)
		{
			NextNode = MainMenuLoading.Instance;
		}

		public override void DoDeactivate()
		{
			base.DoDeactivate();
			UIResult.OnClick -= UIResult_OnClick;
		}
	}
}