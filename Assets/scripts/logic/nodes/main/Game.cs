﻿using System.Collections.Generic;
using components.checkpoints;
using components.scenes;
using components.vehicle;
using logic.core;
using tools;
using ui;
using UnityEngine;

namespace logic.nodes.main
{
	public class Game : GameNode<Game>
	{
		private float _time = 0.0f;
		private SpeedCalculator _speedCalculator = null;
		private Transform _transformVehicle = null;
		private List<Matrix4x4> _track = null;

		public override void DoActivate()
		{
			base.DoActivate();
			UIHUD.OnEscapeClicked += UIHUD_OnEscapeClicked;
			GameObject objectVehicle = GameScene.Instance.InstantiateVehicleOrGetInstance(_vehicle.Prefab);
			_transformVehicle = objectVehicle.transform;
			_speedCalculator = objectVehicle.GetComponent<SpeedCalculator>();
			if (!StartPoint.Instance.IsPassed)
			{
				UIHUD.TimeInSeconds = 0.0f;
				UIHUD.AvailableResult = ResultInStarsType.Zero;
				_time = 0.0f;
				_track = new List<Matrix4x4>();
			}
			FinalPoint.Instance.OnPassed += Instance_OnPassed;
			UIHUD.Show();
		}

		private void UIHUD_OnEscapeClicked(object sender, ClickArgs e)
		{
			Pause.Instance.Switch(this);
		}

		private void Instance_OnPassed(object arg1, System.EventArgs arg2)
		{
			Result.Instance.ResultInfo = new data.LevelResult
			{
				Time = _time,
				Track = _track.ToArray()
			};
			Result.Instance.Switch(this);
		}

		public override ILogicNode DoUpdate(float dt)
		{
			UpdateTime(dt);
			UpdateSpeed();
			return base.DoUpdate(dt);
		}

		private void UpdateTime(float dt)
		{
			if (!FinalPoint.Instance.IsPassed && StartPoint.Instance.IsPassed)
			{
				_time += dt;
				UIHUD.TimeInSeconds = _time;
				UIHUD.AvailableResult = ValuesConvertor.TimeToStars(_level, _time);
			}
		}

		private void UpdateSpeed()
		{
			UIHUD.Speed = ValuesConvertor.MetersPerSecondToMilesPerHour(_speedCalculator.SpeedMetersPerSecond);
		}

		public override void DoDeactivate()
		{
			base.DoDeactivate();
			_speedCalculator = null;
			_transformVehicle = null;
		}
	}
}