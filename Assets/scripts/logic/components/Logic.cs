﻿using System.Collections.Generic;
using logic.core;
using logic.core.components;

namespace logic.components
{
	public class Logic : AbstractLogic
	{
		protected override IEnumerable<ILogicNode> GetEntrancePoints()
		{
			return new List<ILogicNode>()
			{
				nodes.main.EntranceNode.Instance
			};
		}
	}
}