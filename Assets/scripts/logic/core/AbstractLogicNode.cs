﻿using logic.core.components;
using System;

namespace logic.core
{
	public abstract class AbstractLogicNode<T> : ILogicNode where T : AbstractLogicNode<T>, new()
	{
		private static T s_instance = null;

		public static T Instance
		{
			get
			{
				if (s_instance == null)
				{
					s_instance = new T();
				}
				return s_instance;
			}
		}

		protected bool IsFrozen
		{
			get
			{
				bool result = AbstractLogic.Instance.IsFrozen(m_layer);
				if (result)
				{
					UnityEngine.Debug.LogWarning(this.ToString() + " check self frozen status while it frozen");
				}
				return result;
			}
		}

		private LayerType m_layer = LayerType.Main;

		public LayerType Layer { get { return m_layer; } private set { m_layer = value; } }

		public AbstractLogicNode(LayerType layer) { Layer = layer; if (s_instance != null) throw new InvalidOperationException(); }

		public abstract void DoActivate();
		public abstract void DoDeactivate();
		public abstract ILogicNode DoUpdate(float dt);

		public virtual void Freze() { }
		public virtual void Unfreze() { }
	}
}