﻿namespace logic.core
{
	public interface ILogic
	{
		void FreezeLayer(LayerType layer, ILogicNode freezer);
		void UnfrezeLayer(LayerType layer, ILogicNode freezer);
		bool IsFrozen(LayerType layer);
		void Update();
	}
}
