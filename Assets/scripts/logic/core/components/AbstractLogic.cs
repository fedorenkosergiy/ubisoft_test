﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace logic.core.components
{
	public abstract class AbstractLogic : MonoBehaviour, ILogic
	{
		private static AbstractLogic s_instance = null;
		[SerializeField]
		private List<LayerType> m_earlyUpdateOrder = null;
		[SerializeField]
		private List<LayerType> m_lateUpdateOrder = null;

		protected List<ILogicNode> m_entrancePoints = null;

		private Dictionary<LayerType, ILogicNode> m_activeNodes = new Dictionary<LayerType, ILogicNode>();
		private List<LayerType> m_order = new List<LayerType>();

		private Dictionary<LayerType, ILogicNode> m_frozenLayers = new Dictionary<LayerType, ILogicNode>();

		public static ILogic Instance
		{
			get
			{
				return s_instance;
			}
		}

		protected virtual bool TryToSwitch(ILogicNode node)
		{
			bool result = false;
			if (node != null)
			{
				if (m_activeNodes.ContainsKey(node.Layer))
				{
					m_activeNodes[node.Layer].DoDeactivate();
					m_activeNodes[node.Layer] = node;
					node.DoActivate();
				}
				else
				{
					m_activeNodes.Add(node.Layer, node);
					node.DoActivate();
				}
				result = true;
			}
			return result;
		}

		protected virtual int CompareEntrancePointsOrder(ILogicNode a, ILogicNode b)
		{
			return m_order.IndexOf(a.Layer).CompareTo(m_order.IndexOf(b.Layer));
		}

		protected abstract IEnumerable<ILogicNode> GetEntrancePoints();

		protected virtual void Awake()
		{
			if (s_instance == null)
			{
				s_instance = this;
				m_entrancePoints = new List<ILogicNode>(GetEntrancePoints());
				DontDestroyOnLoad(gameObject);
			}
			else
			{
				Destroy(gameObject);
			}
		}

		protected virtual void Start()
		{
			Array allLayers = Enum.GetValues(typeof(LayerType));
			List<LayerType> layers = new List<LayerType>();
			for (int i = 0; i < allLayers.Length; ++i)
			{
				layers.Add((LayerType)allLayers.GetValue(i));
			}

			//early
			for (int i = 0; i < m_earlyUpdateOrder.Count; ++i)
			{
				m_order.Add(m_earlyUpdateOrder[i]);
				layers.Remove(m_earlyUpdateOrder[i]);
			}

			//undefined
			for (int i = 0; i < layers.Count; ++i)
			{
				if (!m_lateUpdateOrder.Contains(layers[i]))
				{
					m_order.Add(layers[i]);
				}
			}

			//late
			for (int i = 0; i < m_lateUpdateOrder.Count; ++i)
			{
				m_order.Add(m_lateUpdateOrder[i]);
			}

			m_entrancePoints.Sort(CompareEntrancePointsOrder);

			for (int i = 0; i < m_entrancePoints.Count; ++i)
			{
				TryToSwitch(m_entrancePoints[i]);
			}
			Update();
		}

		public virtual void Update()
		{
			for (int i = 0; i < m_order.Count; ++i)
			{
				LayerType layer = m_order[i];
				if (!IsFrozen(layer))
				{
					if (m_activeNodes.ContainsKey(layer))
					{
						TryToSwitch(m_activeNodes[layer].DoUpdate(Time.deltaTime));
					}
				}
			}
		}

		public virtual void FreezeLayer(LayerType layer, ILogicNode freezer)
		{
			if (!IsFrozen(layer))
			{
				m_frozenLayers.Add(layer, freezer);
				m_activeNodes[layer].Freze();
			}
			else
			{
				Debug.LogError("Logic error: layer " + layer.ToString() + " already frozen by " + m_frozenLayers[layer].ToString() + ", so " + freezer.ToString() + " can not freeze it");
			}
		}

		public virtual void UnfrezeLayer(LayerType layer, ILogicNode freezer)
		{
			if (IsFrozen(layer))
			{
				if (m_frozenLayers[layer] == freezer)
				{
					m_frozenLayers.Remove(layer);
					m_activeNodes[layer].Unfreze();
				}
				else
				{
					Debug.LogError("Logic error: layer " + layer.ToString() + " frozen by " + m_frozenLayers[layer].ToString() + ", so " + freezer.ToString() + " can not unfreeze it");
				}
			}
			else
			{
				Debug.LogError("Logic error: layer " + layer.ToString() + " not frozen");
			}
		}

		public virtual bool IsFrozen(LayerType layer)
		{
			return m_frozenLayers.ContainsKey(layer);
		}
	}
}