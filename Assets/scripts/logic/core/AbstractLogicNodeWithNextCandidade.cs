﻿namespace logic.core
{
	public abstract class AbstractLogicNodeWithNextCandidade<T> : AbstractLogicNode<T> where T : AbstractLogicNodeWithNextCandidade<T>, new()
	{
		private ILogicNode m_nextNode = null;

		protected virtual ILogicNode NextNode
		{
			get
			{
				return m_nextNode;
			}
			set
			{
				if (m_nextNode != value)
				{
					m_nextNode = value;
				}
			}
		}

		public AbstractLogicNodeWithNextCandidade(LayerType layer) : base(layer) { }
		
		public override ILogicNode DoUpdate(float dt)
		{
			if (IsFrozen)
			{
				return null;
			}
			return NextNode;
		}

	}
}