﻿namespace logic.core
{
	public interface ILogicNode
	{
		LayerType Layer { get; }
		ILogicNode DoUpdate(float dt);
		void DoActivate();
		void DoDeactivate();
		void Freze();
		void Unfreze();
	}
}