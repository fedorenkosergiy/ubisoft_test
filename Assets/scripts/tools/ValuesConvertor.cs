﻿using data;
using ui;

namespace tools
{
	public static class ValuesConvertor
	{
		private const float METERS_PER_MILE = 1609.344f;
		private const float MILES_PER_METER = 6.21371192237334e-4f;
		private const float SECONDS_PER_HOUR = 3600.0f;
		private const float HOURS_PER_SECOND = 2.777777777777778e-4f;

		public static float MetersPerSecondToMilesPerHour(float metersPerSecond)
		{
			return metersPerSecond * SECONDS_PER_HOUR * MILES_PER_METER;
		}

		public static float MilesPerHourToMetersPerSecond(float mph)
		{
			return mph * METERS_PER_MILE * HOURS_PER_SECOND;
		}

		public static ResultInStarsType TimeToStars(Level level, float time)
		{
			ResultInStarsType result = ResultInStarsType.Zero;
			if (time > level.SilverTimeLimit)
			{
				result = ResultInStarsType.One;
			}
			else if (time > level.GoldTimeLimit)
			{
				result = ResultInStarsType.Two;
			}
			else
			{
				result = ResultInStarsType.Three;
			}
			return result;
		}
	}
}