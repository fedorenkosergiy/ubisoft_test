﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tools
{
	public static class Path
	{
		public const string ASSET_EXTENSION = "asset";
		public const string SCENE_EXTENSION = "unity";
		public const char SEPARATOR = '/';

		public static void Prepare(string alreadyPreparedPart, string partShouldBePrepared)
		{
			string path = partShouldBePrepared;
			path = path.Remove(path.Length - System.IO.Path.GetFileName(path).Length);
			string[] parts = path.Split(new char[] { SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);
			Queue<string> partsQueue = new Queue<string>(parts);
			StringBuilder builder = new StringBuilder(alreadyPreparedPart);
			if (partsQueue.Count > 0)
			{
				PrepareNextPath(builder, partsQueue);
			}
		}

		private static void PrepareNextPath(StringBuilder builder, Queue<string> partsQueue)
		{
			string nextPart = partsQueue.Dequeue();
			builder.Append(SEPARATOR);
			builder.Append(nextPart);
			System.IO.Directory.CreateDirectory(builder.ToString());
			if (partsQueue.Count > 0)
			{
				PrepareNextPath(builder, partsQueue);
			}
		}
	}
}